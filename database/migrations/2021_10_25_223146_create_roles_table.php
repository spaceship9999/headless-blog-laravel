<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('role_name');
            $table->boolean('dashboard_access');
            $table->boolean('create_post');
            $table->boolean('edit_post');
            $table->boolean('delete_post');
            $table->boolean('create_reply');
            $table->boolean('edit_reply');
            $table->boolean('delete_reply');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
