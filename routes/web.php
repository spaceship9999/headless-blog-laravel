<?php

use App\Http\Controllers\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['auth', 'canAccessDashboard']], function() {
    Route::get('/', 'App\Http\Controllers\Admin\HomeController@index');

    //Create Post
    Route::get('/create-post', [PostController::class, 'create']);
    Route::post('/create-post', [PostController::class, 'store']);

    //Update Post
    Route::get('/edit-post/{id}', [PostController::class, 'edit']);
    Route::put('/edit-post/{id}', [PostController::class, 'update']);

    //Delete Post
    Route::delete('/delete-post/{id}',[PostController::class, 'destroy']);

});



Route::get('/post', [PostController::class, 'index']);
Route::get('/post/{id}', [PostController::class, 'show']);



