@extends('layout.general_header_footer')

@section('body')
    <div class="container container px-4 lg:px-0 mx-auto">
        <div class="max-w-[500px] mx-auto rounded-lg overflow-hidden my-20 shadow shadow-lg">
            <div class="aspect-w-16 aspect-h-9 bg-gray-400">
                <div class="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 w-20 h-20">
                    <svg xmlns="http://www.w3.org/2000/svg" fill="#fff" class="bi bi-person-circle" viewBox="0 0 16 16">
                        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
                        <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
                    </svg>
                    <div class="text-white text-center text-xl my-4">Login</div>
                </div>

            </div>
            <form class="p-4" method="POST" action="{{ route('login') }}">
                {!! csrf_field() !!}
                <div class="mb-4">
                    <label class="block" for="email">Email</label>
                    <input class="block w-full border-2 border-gray-400 rounded-lg p-3" type="email" name="email" id="email" value="{{ old('email') }}">
                    @if ($errors->has('email'))
                        <span class="text-red-500 text-sm" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                    @endif
                </div>
                <div class="mb-4">
                    <label class="block" for="password">Password</label>
                    <input class="block w-full border-2 border-gray-400 rounded-lg p-3" type="password" name="password" id="password">
                    @if ($errors->has('password'))
                        <span class="text-red-500 text-sm" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                    @endif
                </div>

                <div class="flex items-center mb-4">
                    <input type="checkbox" id="remember" class="checked:bg-[#05445E] w-5 h-5 rounded-full checked:border-transparent mr-2" name="remember">
                    <label for="remember">Remember me</label>
                </div>

                <div>
                    <button class="p-4 rounded-lg bg-yellow-400" type="submit">Login</button>
                </div>
            </form>
        </div>
    </div>
@endsection
