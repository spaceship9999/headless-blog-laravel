@extends('layout.general_header_footer')

@section('body')
    <div class="py-20 bg-[#232323]">
        <div class="container mx-auto px-4 lg:px-0">
            <h1 class="text-4xl font-bold text-white">Dashboard</h1>
        </div>
    </div>
    <div class="container mx-auto px-4 lg:px-0">
        <div class="grid grid-cols-1 lg:grid-cols-2 gap-8 py-10">
            <div class="w-100 shadow shadow-lg rounded-lg overflow-hidden border-2 border-yellow-400">
                <div class="p-4 border-b-2 border-gray-200">
                    <h2 class="font-bold">At a glance</h2>
                </div>
                <div class="p-4">
                    {{ $post_count }} posts
                </div>
            </div>
            <!-- Recent Posts -->
            <div class="w-100 shadow shadow-lg rounded-lg overflow-hidden border-2 border-green-300">
                <div class="p-4 border-b-2 border-gray-200">
                    <h2 class="font-bold">Recent posts</h2>
                </div>
                <div class=" max-h-[300px] overflow-auto">
                    @foreach ($recent_articles as $index => $article)
                        <a href="#" class="block p-4 border-b-2 border-gray-200">
                            {{ $article['title'] }}
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
@endsection
