@extends('layout.general_header_footer')

@section('body')
    <div class="editor-app">
        <form method="POST" class="" action="{{ url('/edit-post/'. $content['id'] ) }}">
            <div class="py-20 bg-[#05445E]">
                <div class="container px-4 lg:px-0 mx-auto">
                    <input type="text" name="title"
                           value="{{ $content['title'] }}"
                           placeholder="Enter title here..."
                           class="p-4 bg-transparent w-full border-0 border-b-2 placeholder-[#fff]
                           border-gray-200 text-white text-3xl">
                </div>
            </div>
            <editor-content></editor-content>
        </form>
    </div>
    <script src="{{asset('/js/app.js') }}"></script>
    <script>
        let base_url = '{{ url('/') }}';
    </script>
@endsection
