<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ url('/public/css/app.css') }}">
</head>
<body class="bg-[#f7f7f7]">
    <div class="container px-4 lg:px-0 mx-auto  my-4">
        <div class=" bg-[#05445E] text-white font-bold rounded-full">
            <a class="block p-4" href="{{ url('/') }}">Raynold Headless Blog</a>
        </div>
    </div>
    @yield('body')
</body>
</html>
