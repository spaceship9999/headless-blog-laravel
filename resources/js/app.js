require('./bootstrap');

import Vue from 'vue';
import { Editor, EditorContent } from '@tiptap/vue-3'
import StarterKit from '@tiptap/starter-kit'

const editor_app = new Vue({
    el: '.editor-app',
    components: {
        EditorContent,
    },
    data() {
        return {
            editor_content: document.getElementById('post_content').innerHTML,
            editor:  null,
        }
    },
    mounted() {
        this.editor = new Editor({
            content: this.editor_content,
            extensions : [
                StarterKit,
            ]
        })
    }
})
