<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function index()
    {
        $post_count = Post::postCount();
        $recent_articles = Post::recentArticles(10);

        return view('home.index', compact('post_count', 'recent_articles'));
    }
}
