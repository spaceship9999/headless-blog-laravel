<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    public static function canAccessDashboard($user_id)
    {
        return self::leftJoin('users', 'users.role_id', '=', 'roles.id')
            ->where('dashboard_access', 1)
            ->where('users.id', $user_id)
            ->exists();
    }

    public static function canCreatePosts($user_id)
    {
        return self::leftJoin('users', 'users.roles_id', '=', 'roles.id')
            ->where('create_post', 1)
            ->where('users.id', $user_id)
            ->exists();
    }
}
