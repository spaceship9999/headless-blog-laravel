<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    public static function postCount()
    {
        return self::count();
    }

    public static function recentArticles($num_posts)
    {
        return self::leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->select('posts.id', 'title', 'content', 'users.id', 'users.name', 'users.email',
                'posts.created_at', 'posts.updated_at', 'slug')
            ->orderBy('posts.created_at', 'desc')
            ->take(10)
            ->get();
    }

    public static function getPostById($post_id)
    {
        return self::leftJoin('users', 'users.id', '=', 'posts.user_id')
            ->select('posts.id', 'users.id', 'users.name', 'title', 'content', 'slug');
    }
}
